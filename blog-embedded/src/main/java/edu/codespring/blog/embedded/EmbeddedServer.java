package edu.codespring.blog.embedded;

//import edu.codespring.blog.servlet.BlogPostServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHandler;

public class EmbeddedServer {

    public static void main(String[] args) throws Exception {
        // start Jetty server on port 8080
        Server server = new Server(8080);

        ServletHandler handler = new ServletHandler();
        server.setHandler(handler);

        // map servlet to path "/"
//        handler.addServletWithMapping(BlogPostServlet.class, "/");

        // start server - we can send requests to http://localhost:8080/
        server.start();
        server.join();
    }
}