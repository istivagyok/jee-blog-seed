package edu.codespring.blog;

import edu.codespring.blog.model.UserDTO;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.Objects;

@Path("user")
public class UserRestService {

    @Inject
    private HttpServletRequest httpServletRequest;

    @Inject
    private HttpSession httpSession;

    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(final UserDTO userDTO) {
        try {
            if (Objects.nonNull(httpServletRequest.getUserPrincipal())) {
                logout();
            }

            httpServletRequest.login(userDTO.getUsername(), userDTO.getPassword());
        } catch (ServletException e) {
            return Response.status(Status.UNAUTHORIZED).build();
        }
        return Response.ok().build();
    }

    @POST
    @Path("logout")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response logout() {
        try {
            httpServletRequest.logout();
            httpSession.invalidate();
        } catch (ServletException e) {
            return Response.serverError().build();
        }
        return Response.ok().build();
    }
}
