package edu.codespring.blog;

import edu.codespring.blog.model.BlogPost;
import edu.codespring.blog.service.BlogPostService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;

@Path("blog")
public class BlogPostRestService {

    @Inject
    private BlogPostService blogPostService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllBlogPosts() {
        final Collection<BlogPost> allBlogPosts = blogPostService.findAllBlogPosts();

        return Response.ok(allBlogPosts).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBlogPostById(@PathParam("id") Long id) {
        final BlogPost blogPostById = blogPostService.findBlogPostById(id);

        return Response.ok(blogPostById).build();
    }

    @GET
    @Path("title/{title}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBlogPostByTitle(@PathParam("title") String title) {
        final Collection<BlogPost> blogPostByTitle = blogPostService.findBlogPostByTitle(title);

        return Response.ok(blogPostByTitle).build();
    }

    @POST
    @Path("create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createBlogPost(final BlogPost blogPost) {
        final BlogPost createdBlogPost = blogPostService.createBlogPost(blogPost);
        return Response.ok(createdBlogPost).build();
    }

    @POST
    @Path("update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateBlogPost(final BlogPost blogPost) {
        final BlogPost updateBlogPost = blogPostService.updateBlogPost(blogPost);
        return Response.ok(updateBlogPost).build();
    }
}
