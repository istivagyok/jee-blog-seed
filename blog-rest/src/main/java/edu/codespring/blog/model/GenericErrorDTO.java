package edu.codespring.blog.model;

import java.io.Serializable;

public class GenericErrorDTO implements Serializable {
    private  String errorMessage;

    public GenericErrorDTO() {
    }

    public GenericErrorDTO(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
