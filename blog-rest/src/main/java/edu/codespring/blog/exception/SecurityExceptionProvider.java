package edu.codespring.blog.exception;

import edu.codespring.blog.model.GenericErrorDTO;

import javax.ejb.EJBAccessException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class SecurityExceptionProvider implements ExceptionMapper<EJBAccessException> {

    @Override
    public Response toResponse(EJBAccessException exception) {
        return Response.status(Response.Status.UNAUTHORIZED).entity(new GenericErrorDTO("Forbidden")).build();
    }
}
