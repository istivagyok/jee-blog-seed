package edu.codespring.blog.service.impl;

class SecurityRoles {
    static final String READ = "read";
    static final String WRITE = "write";
}
