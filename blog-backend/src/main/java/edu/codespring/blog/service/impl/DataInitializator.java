package edu.codespring.blog.service.impl;

import edu.codespring.blog.dao.BlogPostDao;
import edu.codespring.blog.model.BlogPost;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Singleton
@Startup
public class DataInitializator {
    @Inject
    private BlogPostDao blogPostDao; /* The DAO is used instead of the Service because Wildfly does not support javax.annotation.security.RunAs*/

    @PostConstruct
    private void init() {
        final BlogPost blogPost = new BlogPost();
        blogPost.setTitle("testTitle");
        blogPost.setContent("testContent");
        blogPost.setUploadDate(Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC)));
        blogPost.setWriter("testWriter");

        blogPostDao.create(blogPost);
    }
}
