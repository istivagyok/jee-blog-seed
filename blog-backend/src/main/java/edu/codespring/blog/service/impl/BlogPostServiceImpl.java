package edu.codespring.blog.service.impl;

import edu.codespring.blog.dao.BlogPostDao;
import edu.codespring.blog.dao.RepositoryException;
import edu.codespring.blog.model.BlogPost;
import edu.codespring.blog.service.BlogPostService;
import edu.codespring.blog.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Collection;

@Stateless
@DeclareRoles({SecurityRoles.READ, SecurityRoles.WRITE})
public class BlogPostServiceImpl implements BlogPostService {
    private static final Logger LOG = LoggerFactory.getLogger(BlogPostServiceImpl.class);

    @Inject
    private BlogPostDao blogPostDao;

    @RolesAllowed(SecurityRoles.READ)
    @Override
    public Collection<BlogPost> findAllBlogPosts() throws ServiceException {
        try {
            return blogPostDao.findAll();
        } catch (RepositoryException e) {
            final String errorMessage = "findAllBlogPosts failed";
            LOG.error(errorMessage, e);
            throw new ServiceException(errorMessage, e);
        }
    }

    @RolesAllowed(SecurityRoles.READ)
    @Override
    public BlogPost findBlogPostById(Long id) throws ServiceException {
        try {
            return blogPostDao.findById(id);
        } catch (RepositoryException e) {
            final String errorMessage = "findBlogPostById failed";
            LOG.error(errorMessage, e);
            throw new ServiceException(errorMessage, e);
        }
    }

    @RolesAllowed(SecurityRoles.WRITE)
    @Override
    public BlogPost createBlogPost(BlogPost blogPost) throws ServiceException {
        try {
            return blogPostDao.create(blogPost);
        } catch (RepositoryException e) {
            final String errorMessage = "createBlogPost failed";
            LOG.error(errorMessage, e);
            throw new ServiceException(errorMessage, e);
        }
    }

    @RolesAllowed(SecurityRoles.WRITE)
    @Override
    public BlogPost updateBlogPost(BlogPost blogPost) throws ServiceException {
        try {
            return blogPostDao.update(blogPost);
        } catch (RepositoryException e) {
            final String errorMessage = "updateBlogPost failed";
            LOG.error(errorMessage, e);
            throw new ServiceException(errorMessage, e);
        }
    }

    @RolesAllowed(SecurityRoles.READ)
    @Override
    public Collection<BlogPost> findBlogPostByTitle(String title) throws ServiceException {
        try {
            return blogPostDao.findByTitle(title);
        } catch (RepositoryException e) {
            final String errorMessage = "findBlogPostByTitle failed";
            LOG.error(errorMessage, e);
            throw new ServiceException(errorMessage, e);
        }
    }
}
