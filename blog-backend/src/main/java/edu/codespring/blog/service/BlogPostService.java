package edu.codespring.blog.service;

import edu.codespring.blog.model.BlogPost;

import java.util.Collection;

public interface BlogPostService {

    Collection<BlogPost> findAllBlogPosts() throws ServiceException;

    BlogPost findBlogPostById(Long id) throws ServiceException;

    BlogPost createBlogPost(BlogPost blogPost) throws ServiceException;

    BlogPost updateBlogPost(BlogPost blogPost) throws ServiceException;

    Collection<BlogPost> findBlogPostByTitle(String title) throws ServiceException;
}
