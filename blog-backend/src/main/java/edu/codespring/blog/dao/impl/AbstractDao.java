package edu.codespring.blog.dao.impl;

import edu.codespring.blog.dao.Dao;
import edu.codespring.blog.dao.RepositoryException;
import edu.codespring.blog.model.BaseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.Collection;

public class AbstractDao<T extends BaseEntity<ID>, ID> implements Dao<T, ID> {
    private static final Logger LOG = LoggerFactory.getLogger(AbstractDao.class);
    private final Class<T> jpaEntity;

    @PersistenceContext(unitName = "blogPu")
    private EntityManager entityManager;

    AbstractDao(Class<T> jpaEntity) {
        this.jpaEntity = jpaEntity;
    }

    @Override
    @Transactional
    public T create(T obj) throws RepositoryException {
        entityManager.persist(obj);
        entityManager.flush();

        final ID id = obj.getId();
        LOG.info("Created {} with ID {}", obj.getClass().getSimpleName(), id);
        return obj;
    }

    @Override
    @Transactional
    public T update(T obj) throws RepositoryException {
        return entityManager.merge(obj);
    }

    @Override
    public Collection<T> findAll() throws RepositoryException {
        final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<T> query = criteriaBuilder.createQuery(this.jpaEntity);
        final Root<T> from = query.from(this.jpaEntity);
        final CriteriaQuery<T> select = query.select(from);
        return entityManager.createQuery(select).getResultList();
    }

    @Override
    public T findById(Long id) throws RepositoryException {
        return entityManager.find(this.jpaEntity, id);
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }
}
