package edu.codespring.blog.dao.impl;

import edu.codespring.blog.dao.BlogPostDao;
import edu.codespring.blog.model.BlogPost;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;

@ApplicationScoped
public class BlogPostDaoImpl extends AbstractDao<BlogPost, Long> implements BlogPostDao {

    public BlogPostDaoImpl() {
        super(BlogPost.class);
    }

    /**
     * @param title
     * @return
     */
    @Override
    public Collection<BlogPost> findByTitle(String title) {
        final CriteriaBuilder criteriaBuilder = this.getEntityManager().getCriteriaBuilder();
        final CriteriaQuery<BlogPost> query = criteriaBuilder.createQuery(BlogPost.class);
        final Root<BlogPost> from = query.from(BlogPost.class);
        final CriteriaQuery<BlogPost> where = query.select(from)
                                                   .where(criteriaBuilder.equal(from.get(BlogPost.TITLE_FIELD_NAME), title));

        return this.getEntityManager().createQuery(where).getResultList();
    }
}
