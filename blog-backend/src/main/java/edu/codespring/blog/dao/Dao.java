package edu.codespring.blog.dao;

import edu.codespring.blog.model.BaseEntity;

import java.util.Collection;

/**
 * General "data access object" interface.
 * Provides common CRUD methods for entities.
 */
public interface Dao<T extends BaseEntity<ID>, ID> {

    T create(T obj) throws RepositoryException;

    T update(T obj) throws RepositoryException;

    Collection<T> findAll() throws RepositoryException;

    T findById(Long id) throws RepositoryException;
}
