package edu.codespring.blog.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "blog_post")
public class BlogPost extends BaseEntity<Long> {

    public static final String TITLE_FIELD_NAME = "title";

    @Column(name = "title")
    private String title;

    @Column(name = "content")
    private String content;

    @Column(name = "upload_date")
    private Date uploadDate;

    @Column(name = "writer")
    private String writer;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    @Override
    public String toString() {
        return "BlogPost{" +
               "id=" + id +
               ", title='" + title + '\'' +
               ", content='" + content + '\'' +
               ", uploadDate=" + uploadDate +
               ", writer='" + writer + '\'' +
               '}';
    }
}
